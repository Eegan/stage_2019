#include<iostream>
#include "Instance_multi_cdd.h"
#include <vector>
#include <sstream>
#include <istream>
#include <fstream>
#include <limits>
#include <algorithm>
using namespace std;

//-------------------------------------------------------------------
//mini fonction pour comparer deux couples selon leurs premières composantes
//-------------------------------------------------------------------
bool ma_comp(pair<float,int> couple_1 ,pair<float,int> couple_2){
	return (couple_1.first > couple_2.first);
}

//Attributs//

// n: nombre de tâches
// m: nombre de machines
// p: processing time d'une tâche sur une machine
// d: due date d'une machine
// alpha: pénalité d'avance d'une tâche sur une machine
// beta: pénalité de retard d'une tâche sur une machine
// tri_alpha : les indices des tâches triées par ratio alpha_i/p_i décroissant pour chaque machine
// tri_beta  : les indices des tâches triées par ratio beta_i/p_i décroissant pour chaque machine

Instance_multi_cdd::Instance_multi_cdd (int nt, int ma){
	n=nt;
	m=ma;
	p_total=0;
	//initialisation des variables
	p.resize(n);
	alpha.resize(n);
	beta.resize(n);
	trie_alpha.resize(n);
	trie_beta.resize(n);
	d.resize(m,0);
	for(int i=0;i<n;i++){
		p[i].resize(m,0);
		alpha[i].resize(m,0);
		beta[i].resize(m,0);
		trie_alpha[i].resize(m,0);
		trie_beta[i].resize(m,0);
	}
}

void Instance_multi_cdd::showInfo(){

	string filename = "data_instance.txt";
	ofstream data_instance(filename.c_str());

	data_instance << "Nombre de tache: "<< n << endl;
	data_instance << "Nombre de machine: "<< m << endl;
	data_instance << "Due dates: ";
	for(int l=0;l<m;l++){
		data_instance << "d_" << l << ":" << d[l] << " ";
	}
	data_instance << endl;
	data_instance << "Processing time: " << endl;
	for(int i=0;i<n;i++){
		cout << "\t";
		for(int l=0; l<m;l++){
			data_instance << "p_" << i << "_" << l << ":" <<p[i][l] << "   ";
		}
		data_instance << endl;
	}
	data_instance << endl;
	data_instance << "Alpha: " << endl;
	for(int i=0;i<n;i++){
		cout << "\t";
		for(int l=0; l<m;l++){
			data_instance << "a_" << i << "_" << l << ":" <<alpha[i][l] << "   ";
		}
		data_instance << endl;
	}
	data_instance << endl;
	data_instance << "Beta: " << endl;
	for(int i=0;i<n;i++){
		cout << "\t";
		for(int l=0; l<m;l++){
			data_instance << "b_" << i << "_" << l << ":" <<beta[i][l] << "   ";
		}
		data_instance << endl;
	}
	data_instance << endl;
	data_instance << "Trie alpha: " << endl;
	for(int i=0;i<n;i++){
		cout << "\t";
		for(int l=0; l<m;l++){
			data_instance << "ta_" << i << "_" << l << ":" <<trie_alpha[i][l] << "   ";
		}
		data_instance << endl;
	}
	data_instance << endl;
	data_instance << "Trie beta: " << endl;
	for(int i=0;i<n;i++){
		cout << "\t";
		for(int l=0; l<m;l++){
			data_instance << "tb_" << i << "_" << l << ":" <<trie_beta[i][l] << "   ";
		}
		data_instance << endl;
	}
	data_instance << endl;
	data_instance.close();
}

void Instance_multi_cdd::getData(vector<int> num_file){
	stringstream name;
	//récupère les valeurs p, beta, alpha, chaque fichier correspond à une machine
        for(int l=0; l<num_file.size(); l++){
		name.str("");
        name << "../data/sch" << n << "_" << num_file[l] << ".txt";
		string filename= name.str();
		fstream file(filename.c_str(), ios_base::in);

		if(file.is_open()){

			//la 1ère valeur étant le nombre de tâches, pas nécessaire ici, on la met dans une autre variable
			float buffer;
			file >> buffer;
			for(int i=0;i<n;i++){
                                file >> p[i][l];
                                file>>alpha[i][l];
                                file>>beta[i][l];
                                p_total+=p[i][l];
			}
		}
		else{
			cerr << "Fichier " << filename << " introuvable" << endl;
			exit(1);
		}
	}
	for(int l=0; l<m;l++){
		d[l]=p_total;
	}
}
void Instance_multi_cdd::triAlpha(){
	vector<pair<float,int> > pre_res;
	for(int l=0; l<m; l++){
		pre_res.clear();
	 	for (int i=0;i<n;i++){
	 		pair<float,int> ma_tache;
	 		//le ratio
	 		if (p[i][l]>0){
				ma_tache.first= alpha[i][l]/p[i][l];
			}
			else{
				ma_tache.first = std::numeric_limits<float>::max() ;
			}
			//l'indice
			ma_tache.second=i;
			pre_res.push_back(ma_tache);
		}
		//le tri en lui même
		sort(pre_res.begin(),pre_res.end(),ma_comp);
		//récupère les indices
		for (int i=0;i<n;i++)
			trie_alpha[i][l]=pre_res[i].second;
	} 
}


void Instance_multi_cdd::triBeta(){
	vector<pair<float,int> > pre_res;
	for(int l=0; l<m; l++){
		pre_res.clear();
	 	for (int i=0;i<n;i++){
	 		pair<float,int> ma_tache;
	 		//le ratio
	 		if (p[i][l]>0){
				ma_tache.first= beta[i][l]/p[i][l];
			}
			else{
				ma_tache.first = std::numeric_limits<float>::max() ;
			}
			//l'indice
			ma_tache.second=i;
			pre_res.push_back(ma_tache);
		}
		//le tri en lui même
		sort(pre_res.begin(),pre_res.end(),ma_comp);
		//récupère les indices
		for (int i=0;i<n;i++)
			trie_beta[i][l]=pre_res[i].second;
	}
}
