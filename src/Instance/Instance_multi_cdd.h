#ifndef INSTANCE_MULTI_CDD
#define INSTANCE_MULTI_CDD
#include <vector>

using namespace std;

//pour la comparaison de tâches
bool ma_comp (pair<float,int> couple_1 ,pair<float,int> couple_2);

class Instance_multi_cdd{
	public:


	////ATTRIBUTS////

	int n, m;//n taches, m machines
	float p_total;
	vector<vector<float> > p;//processing time
	//penalités avance, retard de chaque tâche sur chaque machine
	vector<vector<float> > alpha;
	vector<vector<float> > beta;
	vector<float> d;//due date
	//trie decroissant par alpha_i/p_i, beta_i/p_i pour chaque machine
	vector<vector<int> > trie_alpha;
	vector<vector<int> > trie_beta;

	////METHODES////

	//constructeur
	Instance_multi_cdd(int n, int m);

	//affichage
	void showInfo();

	//récupération des données
	void getData(vector<int> num_file);

	//tri
	void triAlpha();
	void triBeta();
};

#endif
