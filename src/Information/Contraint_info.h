#ifndef CONTRAINT_INFO_H
#define CONTRAINT_INFO_H

#include <vector>
#include <ilcplex/ilocplex.h>


using namespace std;

class Contraint_info{
	public:

	//Attributs
	IloConstraint* cons;
	int age, i, j, k, l, n_ineq, eta;
	double slack, slack_moy;

	Contraint_info(IloConstraint* p_cons, int ii, int jj, int kk, int ll, int eeta, int nn_ineq);
	void update();
};

#endif
