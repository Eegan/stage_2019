#include<iostream>
#include "Info.h"
using namespace std;

//cette classe permet de transmettre des informations de nos callbacks à notre classe prinicipale

//Attributs//

// nb_cut: nombre de coupes
// temps_user: temps pris par les coupes de l'utilisateur

Info::Info (){
	nb_cut = 0;
	temps_user = 0;
	first_iter = true;
}
