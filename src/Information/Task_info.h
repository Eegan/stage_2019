#ifndef TASK_INFO_H
#define TASK_INFO_H

#include <ilcplex/ilocplex.h>


using namespace std;

class Task_info{
	public:

	//Attributs
	int i;
	double value;

	//Methods
	Task_info(int ii, double vvalue);
};

#endif
