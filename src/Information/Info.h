#ifndef INFO_H
#define INFO_H
#include <vector>
using namespace std;
//cette classe permet de transmettre des informations de nos callbacks à notre fonction principal
class Info{
	public:

	////ATTRIBUTS////

	long nb_cut;
	long temps_user;
	bool first_iter;

	////METHODES////

	//constructeur
	Info();

};

#endif
