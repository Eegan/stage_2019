#include <ilcplex/ilocplex.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <ctime>
#include <list>
#include "../Instance/Instance_multi_cdd.h"
#include "../Separation/Sepa_triangle.h"
#include "../Separation/Sepa_triangle_glouton.h"
#include "../Separation/Sepa_triangle_heuristique.h"
#include "../Information/Info.h"
#include "../Separation/Sepa_clique_heuristique.h"

using namespace std;

int main(int argc, char** argv){
	if(argc<4){
		cerr << "premier argument: type de résolution (n, ns, t, ts, tg, tgs, th, ths, ch, chs)" << endl;
		cerr << "second argument: nombre de tache" << endl;
		cerr << "arguments suivants: numeros de 1 à 10 designant les fichiers utilisés pour les machines" << endl;
		return 1;
	}
	vector<int> num_file;
	string type = argv[1];
	if(type != "n" && type != "ns" && type != "t" && type != "ts" && type != "tg" && type != "tgs" && type != "th" && type != "ths" && type != "ch" && type != "chs"){
		cerr << "premier argument doit être:\n\t n pour lancer normalement" << endl;
		cerr << "\t ns pour lancer normalement sans les contraintes de CPLEX" << endl;
		cerr << "\t t pour lancer avec les contraintes de triangles" << endl;
		cerr << "\t ts pour lancer avec les contraintes de triangles sans les contraintes de CPLEX" << endl;
		cerr << "\t tg pour lancer avec les contraintes de triangles gloutonnes" << endl;
		cerr << "\t tgs pour lancer avec les contraintess de triangles  gloutonnes sans les contraintes de CPLEX" << endl;
		cerr << "\t th pour lancer avec l'heuristique avec les contraintes de triangles" << endl;
		cerr << "\t ths pour lancer avec l'heuristique avec les contraintes de triangles sans les contraintes de CPLEX" << endl;
		cerr << "\t ch pour lancer avec l'heuristique avec les contraintes de cliques" << endl;
		cerr << "\t chs pour lancer avec l'heuristique avec les contraintes de cliques sans les contraintes de CPLEX" << endl;
		return 1;
	}
	int n=std::stoi(argv[2]);
	int m=argc-3;
	int q = 10*n*m;
	for(int i=3; i<argc; i++){
		num_file.push_back(std::atoi(argv[i]));
	}

	Instance_multi_cdd instance (n, m);
	instance.getData(num_file);
	instance.triAlpha();
	instance.triBeta();

	Info info;
	info.temps_user=0;
	info.nb_cut=0;


	//////////////
	//////  CPLEX INITIALIZATION
	//////////////

	IloEnv   env;
	IloModel model(env);

	clock_t debut = clock();

	////////////////////////
	//////  VAR 
	////////////////////////

	//variable delta indique si une tache est executé en avance (en retard) sur une machine.

	vector<vector<IloNumVar>> delta_early, delta_tardy;
	delta_early.resize(n);
	delta_tardy.resize(n);

	for(int j=0;j<n;j++){
		delta_early[j].resize(m);
		delta_tardy[j].resize(m);
		for(int l=0;l<m;l++){
			delta_early[j][l]=IloNumVar(env, 0.0, 1.0, ILOINT);
			ostringstream varname1;
			varname1.str("");
			varname1<<"delta_early_"<<j<<"_"<<l;
			delta_early[j][l].setName(varname1.str().c_str());

			delta_tardy[j][l]=IloNumVar(env, 0.0, 1.0, ILOINT);
			ostringstream varname2;
			varname2.str("");
			varname2<<"delta_tardy_"<<j<<"_"<<l;
			delta_tardy[j][l].setName(varname2.str().c_str());
		}
	}

	//variable x permettant de linéariser le calcule de l'avance et du retard

	vector<vector<vector<IloNumVar>>> x_early, x_tardy;
	x_early.resize(n);
	x_tardy.resize(n);


	for(int i=0;i<n;i++){
		x_early[i].resize(n);
		x_tardy[i].resize(n);
		for(int j=0;j<n;j++){
			x_early[i][j].resize(m);
			x_tardy[i][j].resize(m);
			for(int l=0;l<m;l++){
				x_early[i][j][l]=IloNumVar(env, 0.0, 1.0, ILOFLOAT);
				ostringstream varname1;
				varname1.str("");
				varname1<<"x_early_"<<i<<"_"<<j<<"_"<<l;
				x_early[i][j][l].setName(varname1.str().c_str());

				x_tardy[i][j][l]=IloNumVar(env, 0.0, 1.0, ILOFLOAT);
				ostringstream varname2;
				varname2.str("");
				varname2<<"x_tardy_"<<i<<"_"<<j<<"_"<<l;
				x_tardy[i][j][l].setName(varname2.str().c_str());
			}
		}
	}

	//////////////
	//////  CST
	//////////////

	IloRangeArray CC(env);
	int nbcst=0;


	for(IloInt i=0;i<n;i++){
		//j>i car contraines symétriques
		for(IloInt j=i+1;j<n;j++){
			for(IloInt l=0;l<m;l++){
				IloExpr cst1(env);
				cst1+= - delta_early[i][l] - delta_early[j][l] + x_early[i][j][l];
				CC.add(cst1 <= 0);
				ostringstream nomcst1;
				nomcst1.str("");
				nomcst1<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_1";
				CC[nbcst].setName(nomcst1.str().c_str());
				nbcst++;

				IloExpr cst2(env);
				cst2+= - delta_early[i][l] + delta_early[j][l] - x_early[i][j][l];
				CC.add(cst2 <= 0);
				ostringstream nomcst2;
				nomcst2.str("");
				nomcst2<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_2";
				CC[nbcst].setName(nomcst2.str().c_str());
				nbcst++;

				IloExpr cst3(env);
				cst3+= delta_early[i][l] - delta_early[j][l] - x_early[i][j][l];
				CC.add(cst3 <= 0);
				ostringstream nomcst3;
				nomcst3.str("");
				nomcst3<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_3";
				CC[nbcst].setName(nomcst3.str().c_str());
				nbcst++;

				//////////

				IloExpr cst4(env);
				cst4+= - delta_tardy[i][l] - delta_tardy[j][l] + x_tardy[i][j][l];
				CC.add(cst4 <= 0);
				ostringstream nomcst4;
				nomcst4.str("");
				nomcst4<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_4";
				CC[nbcst].setName(nomcst4.str().c_str());
				nbcst++;

				IloExpr cst5(env);
				cst5+= - delta_tardy[i][l] + delta_tardy[j][l] - x_tardy[i][j][l];
				CC.add(cst5 <= 0);
				ostringstream nomcst5;
				nomcst5.str("");
				nomcst5<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_5";
				CC[nbcst].setName(nomcst5.str().c_str());
				nbcst++;

				IloExpr cst6(env);
				cst6+= delta_tardy[i][l] - delta_tardy[j][l] - x_tardy[i][j][l];
				CC.add(cst6 <= 0);
				ostringstream nomcst6;
				nomcst6.str("");
				nomcst6<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_6";
				CC[nbcst].setName(nomcst6.str().c_str());
				nbcst++;

				//////////

				IloExpr cst7(env);
				cst7+=delta_early[i][l]+delta_tardy[i][l]+delta_early[j][l]+delta_tardy[j][l]-x_early[i][j][l]+x_tardy[i][j][l];
//				cst7+= x_early[i][j][l] + delta_early[i][l] + delta_early[j][l];
				CC.add(cst7<=2);
				ostringstream nomcst7;
				nomcst7.str("");
				nomcst7<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_7";
				CC[nbcst].setName(nomcst7.str().c_str());
				nbcst++;

				IloExpr cst8(env);
				cst8+=delta_early[i][l]+delta_tardy[i][l]+delta_early[j][l]+delta_tardy[j][l]+x_early[i][j][l]-x_tardy[i][j][l];
//				cst8+=delta_tardy[i][l]+delta_tardy[j][l]+x_tardy[i][j][l];
				CC.add(cst8<=2);
				ostringstream nomcst8;
				nomcst8.str("");
				nomcst8<<"CstAux_"<<i<<"_"<<j<<"_"<<l<<"_8";
				CC[nbcst].setName(nomcst8.str().c_str());
				nbcst++;
			}
		}
	}

	// sum_{j=1 to n} delta_early_jl+delta_tardy_jl = 1   for all machine l=1 to m;
	for(int j=0;j<n;j++){
		IloExpr cst_delta(env);
		for(int l=0;l<m;l++){
			cst_delta+=delta_tardy[j][l]+delta_early[j][l];
		}
		CC.add(cst_delta==1);
		ostringstream nomcst;
		nomcst.str("");
		nomcst<<"CstDelta_"<<j;
		CC[nbcst].setName(nomcst.str().c_str());
		nbcst++;
	}


	model.add(CC);


	//////////////
	////// OBJ
	//////////////


	IloExpr objective(env);
	for(int l=0; l<m; l++){
		//somme des pénalités d'avance
		for(int k=0; k<n; k++){
			int i=instance.trie_alpha[k][l];
			for(int kk=0; kk<k; kk++){
				int j=instance.trie_alpha[kk][l];
				if(i<j){
					objective+=instance.alpha[i][l]*instance.p[j][l] * (delta_early[i][l]+delta_early[j][l] - x_early[i][j][l]);
				}
				else{
					objective+=instance.alpha[i][l]*instance.p[j][l] * (delta_early[i][l]+delta_early[j][l] - x_early[j][i][l]);
				}
			}
		}
		//somme des pénalités de retard
		for(int k=0; k<n; k++){
			int i=instance.trie_beta[k][l];
			for(int kk=0; kk<k; kk++){
				int j=instance.trie_beta[kk][l];
				if(i<j){
					objective+=instance.beta[i][l]*instance.p[j][l] * (delta_tardy[i][l]+delta_tardy[j][l] - x_tardy[i][j][l]);
				}
				else{
					objective+=instance.beta[i][l]*instance.p[j][l] * (delta_tardy[i][l]+delta_tardy[j][l] - x_tardy[j][i][l]);
				}
			}
			objective+=2*instance.beta[i][l]* instance.p[i][l]*delta_tardy[i][l];
		}
	}
	objective*=0.5;

	model.add(IloMinimize(env,objective));

	//////////////
	////// RESOLUTION
	//////////////

	IloCplex cplex(model);

	cplex.setParam(IloCplex::TiLim, 3500);

	if( type == "ts" || type == "ns" || type == "tgs" || type == "ths" || type == "chs"){
		cplex.setParam(IloCplex::Cliques,-1);
		cplex.setParam(IloCplex::Covers,-1);
		cplex.setParam(IloCplex::DisjCuts,-1);
		cplex.setParam(IloCplex::FlowCovers,-1);
		cplex.setParam(IloCplex::FlowPaths,-1);
		cplex.setParam(IloCplex::FracCuts,-1);
		cplex.setParam(IloCplex::GUBCovers,-1);
		cplex.setParam(IloCplex::ImplBd,-1);//
		cplex.setParam(IloCplex::MIRCuts,-1);//
		cplex.setParam(IloCplex::ZeroHalfCuts,-1);//
		cplex.setParam(IloCplex::MCFCuts,-1);
		//mon_cplex.setParam(IloCplex::Param::MIP::Cuts::LiftProj,-1);
		//désactivation de l'heuristique primale
		cplex.setParam(IloCplex::HeurFreq,-1);
		cplex.setParam(IloCplex::RINSHeur,-1);
		cplex.setParam(IloCplex::FPHeur,-1);
		//désactivation des réductions primales et duales)
		// cf https://www.ibm.com/support/knowledgecenter/cs/SSSA5P_12.7.0/ilog.odms.cplex.help/CPLEX/Parameters/topics/Reduce.html
		cplex.setParam(IloCplex::Param::Preprocessing::Reduce,0);
		//autre désactivation, qui comme son nom ne l'indique pas, désaactive le réductions non linéaires en pré-processin
		//cf https://www.ibm.com/support/knowledgecenter/SSSA5P_12.6.0/ilog.odms.cplex.help/CPLEX/Parameters/topics/PreLinear.html
		cplex.setParam(IloCplex::Param::Preprocessing::Linear,0);

		cplex.setParam(IloCplex::Param::MIP::Limits::Nodes,0);

	}
	if( type == "t" || type == "ts"){
		IloCplex::Callback CB = new (env) Sepa_triangle(&env, n, m, &x_early, &x_tardy, &info);
		cplex.use(CB);
	}
	if( type == "tg" || type == "tgs"){
		IloCplex::Callback CB = new (env) Sepa_triangle_glouton(&env, n, m, &x_early, &x_tardy, &info, q);
		cplex.use(CB);
	}
	if( type == "th" || type == "ths"){


		list <list< list< list< Contraint_info* >* >* >* >* pool;
		pool = new list <list< list< list< Contraint_info* >* >* >* >;
		list< Contraint_info*>* pool_secondaire = new list < Contraint_info*>;
		list< Contraint_info*>* pool_temp_ajouter = new list < Contraint_info*>;
		list< Contraint_info*>* pool_temp_supprimer = new list < Contraint_info*>;
		for(int l=0; l<m; l++){
			list<list<list<Contraint_info *>*>*>* temp_list;
			temp_list = new list<list<list<Contraint_info *>*>*>;
			for(int i=0; i<4; i++){
				list<list<Contraint_info *>*>* temp_l;
				temp_l = new list<list<Contraint_info *> *>;
				for(int j=0; j<2; j++){
					list<Contraint_info* >* temp_e;
					temp_e = new list<Contraint_info* >;
					temp_l->push_back(temp_e);
				}
				temp_list->push_back(temp_l);
			}
			pool->push_back(temp_list);
		}
		IloCplex::Callback CB = new (env) Sepa_triangle_heuristique(&env, &model, n, m, &x_early, &x_tardy, &info, q, pool, pool_secondaire, pool_temp_ajouter, pool_temp_supprimer);
		cplex.use(CB);
	}
	if(type == "ch" || type == "chs"){
		IloCplex::Callback CB = new (env) Sepa_clique_heuristique(&env, &model, n, m, &x_early, &x_tardy, &delta_early, &delta_tardy, &info);
		cplex.use(CB);
	}

	cplex.exportModel("../out/sortie_compact.lp");
	bool solved = cplex.solve();

	/*if ( !solved ) {
		env.error() << "Failed to optimize LP" << endl;
		exit(1);
	}*/

	clock_t fin=clock();
	double duree_clock = (fin - debut);

	env.out() << "Solution status = " << cplex.getStatus() << endl;
	if(solved){
		env.out() << "Solution value  = " << cplex.getObjValue() << endl;
	}
	else{
		env.out() << "No solution foud" << endl;
	}

	//ecriture des résultats dans un fichier

	string filename = "../out/resultat_compact_"+type+".csv";
	ofstream resultat(filename.c_str(), ios::out | ios::app);

	int length = resultat.tellp();

	//légende du tableau
	if (length == 0){
		resultat << "tache" << "," << "machine" << "," << "," << "statut" << "," << "valeur" << "," << "borne inf" << "," << "gap" << "," << "," << "temps" << "," << "nb-node" << "," << "num fichier";
		if(type.rfind("t", 0) == 0 || type.rfind("c", 0) == 0){
			resultat << "," << "," << "nb separation" << "," << "temps user";
		}
		resultat << "\n\n";
	}

	//données de l'instance
	resultat << n << "," << m << "," << ',' << cplex.getStatus() << ",";

	//valeur de la fonction objective
	if(solved){
		 resultat << cplex.getObjValue() << "," ;
	}
	else{
		 resultat << "-," ;
	}

	//valeur de la born inf
	resultat << cplex.getBestObjValue() << ",";

	//gap
	if (solved){
		resultat << (cplex.getObjValue() - cplex.getBestObjValue())/cplex.getBestObjValue() << ',';
	}
	else{
		resultat << "-,";
	}


	//temps d'execution et nb node
 	resultat << "," << cplex.getTime() << "," << cplex.getNnodes() << ",";

	for(int l=0; l<num_file.size(); l++){
		if(l==num_file.size()-1){
			resultat << num_file[l];
		}
		else{
			resultat << num_file[l] << "-";
		}
	}

	if(type.rfind("t",0) == 0 || type.rfind("c", 0) == 0){
		resultat << "," << "," << info.nb_cut << "," << 100*(info.temps_user/duree_clock) << "%";
	}
 	resultat << "\n";

	resultat.close();

	env.end();
}
