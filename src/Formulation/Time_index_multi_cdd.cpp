#include <ilcplex/ilocplex.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <ctime>
#include "../Instance/Instance_multi_cdd.h"

int main(int argc, char** argv){

	if(argc<3){
		cerr << "premier argument: nombre de tache" << endl;
        cerr << "arguments suivants: numeros de 1 à 10 designant les fichiers utilisés pour les machines" << endl;
		return 1;
	}

	vector<int> num_file;
	int n=std::stoi(argv[1]);
    int m=argc-2;
    for(int i=2; i<argc; i++){
        num_file.push_back(std::atoi(argv[i]));
    }

	Instance_multi_cdd instance (n, m);
    instance.getData(num_file);

	//////////////
	//////  CPLEX INITIALIZATION
	//////////////

	IloEnv   env;
	IloModel model(env);


	////////////////////////
	//////  VAR 
	////////////////////////

	vector<vector<vector<IloNumVar>>> Y;
	Y.resize(n);
	for(int i=0;i<n;i++){
		Y[i].resize(m);
		for(int l=0; l<m; l++){
			Y[i][l].resize(2*instance.d[l]);
			for(int q=0; q<2*instance.d[l]; q++){
				Y[i][l][q]=IloNumVar(env, 0.0, 1.0, ILOINT);
				ostringstream varname;
				varname.str("");
				varname<<"Y_"<<i<<"_"<<l<<"_"<<q;
				Y[i][l][q].setName(varname.str().c_str());
			}
		}
	}

	//////////////
	//////  CST
	//////////////

	IloRangeArray CC(env);
	int nbcst=0;

//NOUVELLES CONTRAINTES
	for(int l=0; l<m; l++){
		for(int q=0; q<2*instance.d[l]; q++){
			IloExpr cst(env);
			for(int j=0;j<n;j++){
				if(q+instance.p[j][l]<2*instance.d[l]){
					for(int k=q+1; k<q+instance.p[j][l]+1; k++){
						cst+=Y[j][l][k];
					}
				}
			}
			CC.add(cst <= 1);
			ostringstream nomcst;
			nomcst.str("");
			nomcst<<"CstMonotache_"<<l<<"_"<<q;
			CC[nbcst].setName(nomcst.str().c_str());
			nbcst++;
		}
	}

	for(int j=0;j<n;j++){
		IloExpr cst(env);
		for(int l=0; l<m; l++){
			for(int q=0; q<2*instance.d[l]; q++){
				cst += Y[j][l][q];

			}
		}
		CC.add(cst == 1);
		ostringstream nomcst;
		nomcst.str("");
		nomcst<<"Cst_uni_"<<j;
		CC[nbcst].setName(nomcst.str().c_str());
		nbcst++;
	}

	model.add(CC);


	//////////////
	////// OBJ
	//////////////


	IloExpr objective(env);

	for(int j=0; j<n; j++){
		for(int l=0; l<m; l++){
			for(int q=0; q<instance.d[l]; q++){
				objective+=instance.alpha[j][l]*Y[j][l][q]*(instance.d[l]-q);
			}
			for(int q=instance.d[l]; q<2*instance.d[l]; q++){
				objective+=instance.beta[j][l]*Y[j][l][q]*(q-instance.d[l]);
			}
		}
	}

	model.add(IloMinimize(env,objective));

	//////////////
	////// RESOLUTION
	//////////////

	IloCplex cplex(model);

	cplex.setParam(IloCplex::TiLim, 3500);

	cplex.exportModel("../out/sortie_time_index.lp");

	bool solved = cplex.solve();
	if ( !solved ) {
    	env.error() << "Failed to optimize LP" << endl;
    	exit(1);
	}



	env.out() << "Solution status = " << cplex.getStatus() << endl;
	env.out() << "Solution value  = " << cplex.getObjValue() << endl;

	//ecriture des résultats dans un fichier
	string filename = "../out/resultat_time_index.csv";
	ofstream resultat(filename.c_str(), ios::out | ios::app);

	int length = resultat.tellp();

	//légende du tableau
	if (length == 0){
		resultat << "tache" << "," << "machine" << "," << "," << "statut" << "," << "valeur" << "," << "borne inf" << "," << "gap" << "," << "," << "temps" << "," << "nb-node" << "," << "num fichier" << "\n\n";
	}

	//données de l'instance
	resultat << n << "," << m << "," << ',' << cplex.getStatus() << ",";
	
	//valeur de la fonction objective
	if(solved){
		 resultat << cplex.getObjValue() << "," ;
	}
	else{
		 resultat << "-," ;
	}

	//valeur de la born inf
	resultat << cplex.getBestObjValue() << ",";

	//gap
	if (solved){
		resultat << (cplex.getObjValue() - cplex.getBestObjValue())/cplex.getBestObjValue() << ',';
	}
	else{
		resultat << "-,";
	}


	//temps d'execution et nb node
 	resultat << "," << cplex.getTime() << "," << cplex.getNnodes() << ",";

	for(int l=0; l<num_file.size(); l++){
		if(l==num_file.size()-1){
			resultat << num_file[l] << "\n";
		}
		else{
			resultat << num_file[l] << "-";
		}
	}
	
	resultat.close();

	env.end();
}
