
#ifndef SEPA_TRIANGLE_HEURISTIQUE_H
#define SEPA_TRIANGLE_HEURISTIQUE_H

#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <ilcplex/ilocplex.h>
#include "../Information/Info.h"
#include "../Information/Contraint_info.h"

using namespace std;


class Sepa_triangle_heuristique : public IloCplex::UserCutCallbackI{
	public:
 		///ATTRIBUTS///
		IloEnv* env;
		IloModel* model;
		int n, m;
		vector<vector<vector<IloNumVar>>>* x_early, *x_tardy;
		Info* info;
		int q;
		list< list< list< list< Contraint_info* >* >* >* >* pool;
		list< Contraint_info* >* pool_secondaire, *pool_temp_supprimer, *pool_temp_ajouter;

		///METHODES///
		//de création
		Sepa_triangle_heuristique(IloEnv* p_env, IloModel* p_model, int nn, int mm, vector<vector<vector<IloNumVar>>>* p_x_early, vector<vector<vector<IloNumVar>>>* p_x_tardy, Info* p_info, int qq, list< list< list< list< Contraint_info* >* >* >* >* p_pool, list< Contraint_info* >* p_pool_secondaire, list< Contraint_info* >* p_pool_temp_ajouter, list< Contraint_info* >* p_pool_temp_supprimer);

		//obligatoires
	protected:
		 IloCplex::CallbackI * duplicateCallback() const;


		//de séparation
		void main();

		//d'ajout de contraintes
		void addRandomConst();

		//création de IloExpr
		IloExpr* getExpr(int i, int j, int k, int l, int n_ineq, int eta);

		//calcul de slack
		double getSlack(int i, int j, int k, int l, int n_ineq, int eta);

		//affichage des contraintes
		void afficherConst();

		//tri d'entiers
		void sortThreeInt(int* i, int* j, int* k);
};

#endif


