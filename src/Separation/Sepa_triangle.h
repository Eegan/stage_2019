#ifndef SEPA_TRIANGLE_H
#define SEPA_TRIANGLE_H

#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <ilcplex/ilocplex.h>
#include "../Information/Info.h"

using namespace std;


class Sepa_triangle : public IloCplex::UserCutCallbackI{
 public:
 	///ATTRIBUTS///
	IloEnv* env;
	int n, m;
	vector<vector<vector<IloNumVar>>>* x_early, *x_tardy;
	Info* info;

	///METHODES///
	//de création
	Sepa_triangle(IloEnv* p_env, int nn, int mm, vector<vector<vector<IloNumVar>>>* p_x_early, vector<vector<vector<IloNumVar>>>* p_x_tardy, Info* p_info);

	//obligatoires
	protected:
		 IloCplex::CallbackI * duplicateCallback() const;


	//de séparation
	protected:
		void main();
};

#endif


