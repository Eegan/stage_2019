
#include<vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "Sepa_triangle_glouton.h"
#include "../Information/Info.h"

Sepa_triangle_glouton::Sepa_triangle_glouton(IloEnv* p_env, int nn, int mm, vector<vector<vector<IloNumVar>>>* p_x_early, vector<vector<vector<IloNumVar>>>* p_x_tardy, Info* p_info, int qq):IloCplex::UserCutCallbackI(*p_env){
	env = p_env;
	n = nn;
	m = mm;
	x_early = p_x_early;
	x_tardy = p_x_tardy;
	info = p_info;
	q = qq;
};

IloCplex::CallbackI * Sepa_triangle_glouton::duplicateCallback() const{
	cerr<<"ici duplicate de sepa_triangle_glouton"<<endl;
	Sepa_triangle_glouton * copie = new Sepa_triangle_glouton(env, n, m, x_early, x_tardy, info, q);
	return copie;
};

void Sepa_triangle_glouton::main(){
	vector<vector<vector<IloNumVar>>> x_e = (*x_early);
	vector<vector<vector<IloNumVar>>> x_t = (*x_tardy);
	clock_t debut;
	double epsilon = 0.000001;
	debut = clock();
	cout << "Debut main sepa_triangle_glouton" << endl;
	srand(time(NULL));
	try{
	while (info->nb_cut <q){
		int l = rand() % m;
		int i = rand() % (n-2);
		int j = rand() % (n-1-(i+1)) + (i+1);
		int k = rand() % (n-(j+1)) + (j+1);
		int num_ineq = rand() % 8;
		cout << i << " " << j << " " << k << " " << l << " " << num_ineq << endl;
		cout << getValue(x_early->at(i)[j][l]) << endl;
		switch(num_ineq){
		case 0:
			if(getValue(x_early->at(i)[j][l]) + getValue(x_early->at(i)[k][l]) + getValue(x_early->at(j)[k][l]) > 2+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+=x_early->at(i)[j][l] + x_early->at(i)[k][l] + x_early->at(j)[k][l];
				info->nb_cut++;
				add(cst_triangle <=2, IloCplex::UseCutPurge);
			}
			break;
		case 1:
			if( - getValue(x_early->at(i)[j][l]) - getValue(x_early->at(i)[k][l]) + getValue(x_early->at(j)[k][l]) > 0+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+= - x_early->at(i)[j][l] - x_early->at(i)[k][l] + x_early->at(j)[k][l];
				add(cst_triangle <=0, IloCplex::UseCutPurge);
				info->nb_cut++;
			}
			break;
		case 2:
			if( - getValue(x_early->at(i)[j][l]) + getValue(x_early->at(i)[k][l]) - getValue(x_early->at(j)[k][l]) > 0+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+= - x_early->at(i)[j][l] + x_early->at(i)[k][l] - x_early->at(j)[k][l];
				add(cst_triangle <=0, IloCplex::UseCutPurge);
				info->nb_cut++;
			}
			break;
		case 3:
			if(getValue(x_early->at(i)[j][l]) - getValue(x_early->at(i)[k][l]) - getValue(x_early->at(j)[k][l]) > 0+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+=x_early->at(i)[j][l] - x_early->at(i)[k][l] - x_early->at(j)[k][l];
				add(cst_triangle <=0, IloCplex::UseCutPurge);
				info->nb_cut++;
			}

			break;
		case 4:
			if(getValue(x_tardy->at(i)[j][l]) + getValue(x_tardy->at(i)[k][l]) + getValue(x_tardy->at(j)[k][l]) > 2+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+=(*x_tardy)[i][j][l] + x_tardy->at(i)[k][l] + x_tardy->at(j)[k][l];
				add(cst_triangle <=2, IloCplex::UseCutPurge);
				info->nb_cut++;
			}
			break;
		case 5:
			if( - getValue(x_tardy->at(i)[j][l]) - getValue(x_tardy->at(i)[k][l]) + getValue(x_tardy->at(j)[k][l]) > 0+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+= - (*x_tardy)[i][j][l] - x_tardy->at(i)[k][l] + x_tardy->at(j)[k][l];
				add(cst_triangle <=0, IloCplex::UseCutPurge);
				info->nb_cut++;
			}
			break;
		case 6:
			if( - getValue(x_tardy->at(i)[j][l]) + getValue(x_tardy->at(i)[k][l]) - getValue(x_tardy->at(j)[k][l]) > 0+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+= - (*x_tardy)[i][j][l] + x_tardy->at(i)[k][l] - x_tardy->at(j)[k][l];
				add(cst_triangle <=0, IloCplex::UseCutPurge);
				info->nb_cut++;
			}
			break;
		case 7:
			if(getValue(x_tardy->at(i)[j][l]) - getValue(x_tardy->at(i)[k][l]) - getValue(x_tardy->at(j)[k][l]) > 0+epsilon){
				IloExpr cst_triangle(*env);
				cst_triangle+=x_tardy->at(i)[j][l] - x_tardy->at(i)[k][l] - x_tardy->at(j)[k][l];
				add(cst_triangle <=0, IloCplex::UseCutPurge);
				info->nb_cut++;
			}
			break;
		}
	}
	}
	catch (IloException &e){
		cerr << e << endl;
		throw e;
	}
	clock_t fin;
	fin=clock();
	int duree_en_clock = (fin - debut);
	info->temps_user+=duree_en_clock;
};
