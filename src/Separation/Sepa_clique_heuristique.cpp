#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <bits/stdc++.h>
#include "Sepa_clique_heuristique.h"
#include "../Information/Info.h"
#include "../Information/Contraint_info.h"
#include "../Information/Task_info.h"


using namespace std;

Sepa_clique_heuristique::Sepa_clique_heuristique(IloEnv* p_env, IloModel* p_model, int nn, int mm, vector<vector<vector<IloNumVar>>>* p_x_early, vector<vector<vector<IloNumVar>>>* p_x_tardy, vector<vector<IloNumVar>>* p_delta_early, vector<vector<IloNumVar>>* p_delta_tardy, Info* p_info):IloCplex::UserCutCallbackI(*p_env){
	env = p_env;
	model = p_model;
	n = nn;
	m = mm;
	x_early = p_x_early;
	x_tardy = p_x_tardy;
	delta_early = p_delta_early;
	delta_tardy = p_delta_tardy;
	info = p_info;
};

IloCplex::CallbackI * Sepa_clique_heuristique::duplicateCallback() const{
//	cerr<<"ici duplicate de sepa_triangle_heuristique"<<endl;
	Sepa_clique_heuristique * copie = new Sepa_clique_heuristique(env, model, n, m, x_early, x_tardy, delta_early, delta_tardy, info);
	return copie;
};

void Sepa_clique_heuristique::main(){

	//pour mesurer le temps passé dans la fonction
	clock_t debut;
	debut=clock();

	double D = 0.5;
	double epsilon = 0.01;

	if(info->nb_cut == 0){
		for(int eta = 0; eta < 2; eta ++){
			vector<vector<vector<IloNumVar>>>* x;
			vector<vector<IloNumVar>>* delta;
			if(eta == 0){
				x = x_early;
				delta = delta_early;
			}
			else{
				x = x_tardy;
				delta = delta_tardy;
			}
			for(int l = 0; l<m; l++){
				list<Task_info*> K;

				//calcule de la valeur: delta_i_l + sum x_i_j_l
				for(int i = 0; i < n; i ++){
					double value = 0;
					value += getValue(delta->at(i)[l]);
					for(int j = 0; j<n; j++){
						if(j > i){
							value += D * getValue(x->at(i)[j][l]);
						}
						if(j < i){
							value += D * getValue(x->at(j)[i][l]);
						}
					}
					Task_info *ti = new Task_info(i, value);
					K.push_back(ti);
				}
				//Tri de la liste par valeur décroissante.
				K.sort([](Task_info* ti1, Task_info* ti2) { return ti1->value > ti2 -> value;});

				//Pour les k taches avec la valeur la plus élevée, ajout de la contrainte de clique si elle est violée
				for(int k=0; k<n; k++){
					for(int gamma = ceil(k/2); gamma<n; gamma++){
						double V = 0;
						//ajout de la valeur des delta
						for(int i=0; i <k; i++){
							auto it_i = next(K.begin(), i);
							int ii = (*it_i)->i;
							V+=(2*gamma - (k-1))*getValue(delta->at(ii)[l]);
						}
						//ajout de la valeur des x
						for(int i=0; i<k; i++){
							for(int j=i; j<k; j++){
								auto it_i = next(K.begin(), i);
								auto it_j = next(K.begin(), j);
								int ii = (*it_i)->i;
								int jj = (*it_j)->i;
								if(ii > jj){
									V+=getValue(x->at(jj)[ii][l]);
								}
								if(ii < jj){
									V+=getValue(x->at(ii)[jj][l]);
								}
							}
						}
						//Si la contrainte est violée, on l'ajoute
						if(V > gamma*(gamma+1) + epsilon){
							IloExpr *cst_clique = new IloExpr(*env);
							//ajout de la valeur des delta
							for(int i=0; i <k; i++){
								auto it_i = next(K.begin(), i);
								int ii = (*it_i)->i;
								*cst_clique+=(2*gamma - (k-1))*delta->at(ii)[l];
							}
							//ajout de la valeur des x
							for(int i=0; i<k; i++){
								for(int j=i; j<k; j++){
									auto it_i = next(K.begin(), i);
									auto it_j = next(K.begin(), j);
									int ii = (*it_i)->i;
									int jj = (*it_j)->i;
									if(ii > jj){
										*cst_clique+=x->at(jj)[ii][l];
									}
									if(ii < jj){
										*cst_clique+=x->at(ii)[jj][l];
									}
								}
							}
							IloConstraint *cons = new IloConstraint();
							*cons = *cst_clique <= gamma*(gamma+1);
							add(*cons, IloCplex::UseCutForce);
							info->nb_cut++;
							cout << *cons << endl;
							cout << "k = " << k << "  gamma = " << gamma << "  2*gamma - (k-1) = " << 2*gamma-(k-1) << endl;
							cout << "value V = " << V << endl << endl;
							k=n;
							gamma = n;
						}
					}
				}
			}
		}
	}

	clock_t fin;
	fin=clock();
	int duree_en_clock = (fin - debut);
	info->temps_user+=duree_en_clock;
	//ajout du temps dans l'objet info
};
