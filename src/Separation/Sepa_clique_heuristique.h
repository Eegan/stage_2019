
#ifndef SEPA_CLIQUE_HEURISTIQUE_H
#define SEPA_CLIQUE_HEURISTIQUE_H

#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <ilcplex/ilocplex.h>
#include "../Information/Info.h"
#include "../Information/Contraint_info.h"

using namespace std;


class Sepa_clique_heuristique : public IloCplex::UserCutCallbackI{
	public:
 		///ATTRIBUTS///
		IloEnv* env;
		IloModel* model;
		int n, m;
		vector<vector<vector<IloNumVar>>>* x_early, *x_tardy;
		vector<vector<IloNumVar>>* delta_early, *delta_tardy;
		Info* info;

		///METHODES///
		//de création
		Sepa_clique_heuristique(IloEnv* p_env, IloModel* p_model, int nn, int mm, vector<vector<vector<IloNumVar>>>* p_x_early, vector<vector<vector<IloNumVar>>>* p_x_tardy, vector<vector<IloNumVar>>* p_delta_early, vector<vector<IloNumVar>>* p_delta_tardy, Info* p_info);

		//obligatoires
	protected:
		 IloCplex::CallbackI * duplicateCallback() const;

		//de séparation
		void main();

};

#endif


