#include<vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include "Sepa_triangle.h"
#include "../Information/Info.h"

Sepa_triangle::Sepa_triangle(IloEnv* p_env, int nn, int mm, vector<vector<vector<IloNumVar>>>* p_x_early, vector<vector<vector<IloNumVar>>>* p_x_tardy, Info* p_info):IloCplex::UserCutCallbackI(*p_env){
	env = p_env;
	n = nn;
	m = mm;
	x_early = p_x_early;
	x_tardy = p_x_tardy;
	info = p_info;
};

IloCplex::CallbackI * Sepa_triangle::duplicateCallback() const{
	cerr<<"ici duplicate de sepa_triangle"<<endl;
	Sepa_triangle * copie = new Sepa_triangle(env, n, m, x_early, x_tardy, info);
	return copie;
};

void Sepa_triangle::main(){

	vector<vector<vector<IloNumVar>>> x_e = (*x_early);
	vector<vector<vector<IloNumVar>>> x_t = (*x_tardy);
	clock_t debut;
	//pour éviter les erreurs d'approximation
	double epsilon = 0.001;
	debut=clock();
	//pour chaque machine l et triplet (i,j,k) i<j<k, on ajoute les contraintes de triangles.
	for(int l=0; l<m; l++){
		for(int i=0; i<n; i++){
			for(int j=i+1; j<n; j++){
				for(int k=j+1; k<n; k++){
					cout << l << " " << i << " " << j << " "  << k << endl;
					//cout << getValue(x_early->at(i)[j][l]) <<" "<< getValue(x_early->at(i)[k][l]) <<" "<< getValue(x_early->at(j)[k][l]) << endl;
					if(getValue(x_early->at(i)[j][l]) + getValue(x_early->at(i)[k][l]) + getValue(x_early->at(j)[k][l]) > 2+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+=x_early->at(i)[j][l] + x_early->at(i)[k][l] + x_early->at(j)[k][l];
						add(cst_triangle <=2, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
					if( - getValue(x_early->at(i)[j][l]) - getValue(x_early->at(i)[k][l]) + getValue(x_early->at(j)[k][l]) > 0+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+= - x_early->at(i)[j][l] - x_early->at(i)[k][l] + x_early->at(j)[k][l];
						add(cst_triangle <=0, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
					if( - getValue(x_early->at(i)[j][l]) + getValue(x_early->at(i)[k][l]) - getValue(x_early->at(j)[k][l]) > 0+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+= - x_early->at(i)[j][l] + x_early->at(i)[k][l] - x_early->at(j)[k][l];
						add(cst_triangle <=0, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
					if(getValue(x_early->at(i)[j][l]) - getValue(x_early->at(i)[k][l]) - getValue(x_early->at(j)[k][l]) > 0+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+=x_early->at(i)[j][l] - x_early->at(i)[k][l] - x_early->at(j)[k][l];
						add(cst_triangle <=0, IloCplex::UseCutPurge);
						info->nb_cut++;
					}

					if(getValue(x_tardy->at(i)[j][l]) + getValue(x_tardy->at(i)[k][l]) + getValue(x_tardy->at(j)[k][l]) > 2+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+=(*x_tardy)[i][j][l] + x_tardy->at(i)[k][l] + x_tardy->at(j)[k][l];
						add(cst_triangle <=2, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
					if( - getValue(x_tardy->at(i)[j][l]) - getValue(x_tardy->at(i)[k][l]) + getValue(x_tardy->at(j)[k][l]) > 0+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+= - (*x_tardy)[i][j][l] - x_tardy->at(i)[k][l] + x_tardy->at(j)[k][l];
						add(cst_triangle <=0, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
					if( - getValue(x_tardy->at(i)[j][l]) + getValue(x_tardy->at(i)[k][l]) - getValue(x_tardy->at(j)[k][l]) > 0+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+= - (*x_tardy)[i][j][l] + x_tardy->at(i)[k][l] - x_tardy->at(j)[k][l];
						add(cst_triangle <=0, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
					if(getValue(x_tardy->at(i)[j][l]) - getValue(x_tardy->at(i)[k][l]) - getValue(x_tardy->at(j)[k][l]) > 0+epsilon){
						IloExpr cst_triangle(*env);
						cst_triangle+=x_tardy->at(i)[j][l] - x_tardy->at(i)[k][l] - x_tardy->at(j)[k][l];
						add(cst_triangle <=0, IloCplex::UseCutPurge);
						info->nb_cut++;
					}
				}
			}
		}
	}
	clock_t fin;
	fin=clock();
	int duree_en_clock = (fin - debut);
	info->temps_user+=duree_en_clock;
};
